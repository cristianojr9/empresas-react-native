import React, { useState } from "react";
import { View, Text, TextInput, Image, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import Button from "../../components/Button";

import api from "../../services/api";

import LogoImg from "../../assets/logo_ioasys.png";

const SignIn = ({ navigation }) => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("Preencha email e senha para continuar!");

  const handleEmailChange = (email) => {
    setEmail(email);
  }

  const handlePasswordChange = (password) => {
    setPassword(password);
  }

  const handleSignInPress = async () => {
    if (!email || !password) {
      setError(true);
    } else {
      try {
        const response = await api.post("/users/auth/sign_in", {
          email,
          password,
        }); 

        await AsyncStorage.setItem("@token", response.headers["access-token"]);
        await AsyncStorage.setItem("@client", response.headers["client"]);
        await AsyncStorage.setItem("@uid", response.headers["uid"]); 

        navigation.navigate("Home");
      } catch (_err) {
        setErrorMessage("Houve um problema com o login, verifique suas credenciais!");
      }
    }
  }

  return (
    <View style={styles.container}>

      <Image source={LogoImg} />

      <Text style={styles.title}>
          Bem-vindo novamente 
      </Text>

      <TextInput 
        style={styles.input}
        placeholder="Email"
        placeholderTextColor="#555"
        autoCapitalize="none"
        autoCorrect={false}
        autoCapitalize="none"
        onChangeText={handleEmailChange}
      />
      <TextInput 
        style={styles.input}
        placeholder="Senha"
        secureTextEntry
        placeholderTextColor="#555"
        onChangeText={handlePasswordChange}
      />

      {error && 
        <Text style={styles.error}>
          {errorMessage}
        </Text>
      }


      <Button onPress={handleSignInPress} />

    </View>
  );
}

export default SignIn;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 30,
  },
  title: {
    color: "#111",
    fontSize: 24,
    fontWeight: "bold"
  },
  input: {
    backgroundColor: "#f5f6fb",
    color: "#111",
    fontSize: 18,
    marginTop: 30,
    borderRadius: 7,
    padding: 16,
  },
  error: {
    color:"#ff3333",
    fontSize: 18,
    marginTop: 10,
  }
});
