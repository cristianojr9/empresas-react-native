import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, FlatList } from 'react-native';

import EnterpriseCard from "../../components/EnterpriseCard";

import api from "../../services/api";

const Home = ({ navigation }) => {
  const [enterprises, setEnterprises] = useState([]);
  
  useEffect(() => {
    async function fetch() {
      await api.get("/enterprises").then((response) => {
        setEnterprises(response.data.enterprises);
      })
    }
    fetch();
  },[]);

  const handleToDetail = (id) => {
    navigation.navigate("Details", { enterpriseId: id });
  }

  const renderEnterprises = (enterprise) => (
    <EnterpriseCard 
      key={enterprise.id.toString()}  
      title={enterprise.enterprise_name} 
      description={enterprise.description} 
      onPress={() => handleToDetail(enterprise.id.toString())}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList 
        data={enterprises}
        renderItem={({ item }) => renderEnterprises(item)}
        keyExtractor={(item, index) => item.id}
      />
    </SafeAreaView>
  );
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#a02281",
  },
});