import React, { useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

import api from "../../services/api";

const EnterpriseDetails = ({ route }) => {
  const { enterpriseId } = route.params;

  const [enterprise, setEnterprise] = useState({});

  useEffect(() => {
    async function fetch() {
      await api.get(`/enterprises/${enterpriseId}`).then((response) => {
        setEnterprise(response.data.enterprise);
      });
    }
    fetch();
  },[enterpriseId]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        {enterprise.enterprise_name}
      </Text>
      <View style={styles.city}>
        <Text style={styles.cityTitle}>
          {enterprise.city} - 
        </Text>
        <Text style={styles.countryTitle}> 
          {enterprise.country}
        </Text>
      </View>
      <Text style={styles.description}>
        {enterprise.description}
      </Text>
      
    </View>
  );
}

export default EnterpriseDetails;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: "#a02281",
    flex: 1,
  },
  city: {
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection:"row",
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#fff",
  },
  cityTitle: {
    fontStyle: "italic",
    color: "#fff",
  },
  countryTitle: {
    fontStyle: "italic",
    marginLeft: 5,
    color: "#fff",
  },
  description: {
    color: "#fff",
  }
})