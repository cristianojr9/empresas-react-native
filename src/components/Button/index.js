import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

// import { Container } from './styles';

const Button = ({ onPress }) => {
  return (
    <TouchableOpacity style={styles.button} activeOpacity={.7} onPress={onPress}>
      <Text style={styles.buttonText}>
        Entrar
      </Text>
    </TouchableOpacity>
  );
}

export default Button;

const styles = StyleSheet.create({
  button: {
    padding: 15,
    alignItems: "center",
    backgroundColor: "#a02281",
    marginTop: 30,
    borderRadius: 7
  }, 
  buttonText: {
    color: '#FFF',
    fontSize: 17,
    fontWeight: 'bold',
  },
});