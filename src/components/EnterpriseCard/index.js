import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

import api from "../../services/api";

const EnterpriseCard = ({ onPress, title, description }) => {
  return (
    <TouchableOpacity 
      onPress={onPress} 
      style={styles.container} 
      activeOpacity={.7}
    >
      <Text style={styles.title}>
        {title}
      </Text>

      <Text>
        {description}
      </Text>
    </TouchableOpacity>
  );
}

export default EnterpriseCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20,
    marginBottom: 7,
    margin: 20,
    borderRadius: 7
  },
  title: {
    fontWeight: 'bold',
  }
});