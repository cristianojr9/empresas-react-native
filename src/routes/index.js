import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from "../views/SignIn";
import Home from "../views/Home";
import EnterpriseDetails from "../views/EnterpriseDetails";

const Stack = createStackNavigator();

const routes = () => {
  return (
    <NavigationContainer> 
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen 
          name="Login" 
          component={SignIn} 
          options={{
            title: "Realize seu login",
          }}

        />
        <Stack.Screen 
          name="Home" 
          component={Home} 
          options={{
            title: ""
          }}
        />
        <Stack.Screen 
          name="Details" 
          component={EnterpriseDetails} 
          options={{
            title: "Detalhes da empresa"
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );;
}

export default routes;
