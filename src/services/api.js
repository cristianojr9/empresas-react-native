import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';

const api = axios.create({
  baseURL: "https://empresas.ioasys.com.br/api/v1"
});

api.interceptors.request.use(async (config) => {
  try {
    const token = await AsyncStorage.getItem('@token');
    const client = await AsyncStorage.getItem('@client');
    const uid = await AsyncStorage.getItem('@uid');

    if (token) {
      config.headers["access-token"] = token;
      config.headers["client"] = client;
      config.headers["uid"] = uid;
    }

    return config;
  } catch (err) {
    alert(err);
  }
});

export default api;