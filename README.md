# Desafio React Native - ioasys

Um aplicativo feito em Expo - React Native, onde você pode realizar login,
visualizar as empresas cadastradas e acessar mais detalhes sobre ela clicando nela.

## Justificativas

Utilizei o expo por se tratar de um app que seria voltado apenas para esse teste,
então achei melhor de fazer os testes localmente.

Bibliotecas utilizadas:

- React-native-async-storage
- React-navigation/native
- Axios
- Expo

Para navegação utilizei a opção de Stack, para facilitar a navegação entre as telas, o Axios
para fazer as requisições a API e o async-storage para manipular os dados vindo do login, como ID.

## Funcionalidades

- Login
- Lista de empresas
- Detalhes da empresa

## Para rodar o projeto

Rode um Yarn para instalar as dependências

```bash
  yarn
```

Agora execute o expo para iniciar o aplicativo localmente

```bash
  expo start
```

## Tecnologias utilizadas

**App:** Javascript, React Native, Expo

**Servidor:** http://empresas.ioasys.com.br

## Authors

- [@cristianojr9](https://www.github.com/cristianojr9)
